import re
import unicodedata
from functools import lru_cache


@lru_cache(maxsize=None)
def confirm_normalize(value: str) -> str:
    normalized = unicodedata.normalize("NFKC", value)
    optimized = re.sub(r"\(.*\)", "", value)
    choice = None
    if normalized == optimized and normalized == value:
        return value
    while choice is None:
        question = (
            f"1: {value}\n"
            f"2: {normalized}\n"
            f"3: {optimized}\n"
            "4: Other\n"
            "Enter your choice:"
        )
        select = input(question)
        if select == "1":
            choice = value
        elif select == "2":
            choice = normalized
        elif select == "3":
            choice = optimized
        elif select == "4":
            choice = input("Enter your value:")
    return choice
