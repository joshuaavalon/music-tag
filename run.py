from os import getenv
from logging import getLogger
from pathlib import Path
from typing import List, Optional
from dotenv import load_dotenv

from appixer.metadata import MusicMeta, FlacMeta, Mp3Meta
from appixer import jlyric

from utils import confirm_normalize


logger = getLogger(__name__)


def main():
    load_dotenv(encoding="utf-8")
    base_dir = Path(getenv("BASE_DIR"))
    files = get_music_files(base_dir)

    metadata_dict = {}

    for file in files:
        music = create_meta(file)
        if music is None:
            logger.error("Cannot recognize file %s", file)
            continue
        music.clear()
        if not music.disc_num:
            music.disc_num = "1"
        if music.artist != "楽器":
            update_jlyric(file, music)
            metadata_dict[music.title] = music
        else:
            update_dict(metadata_dict, music)
        cover = file.parent.joinpath("cover.png")
        if not cover.is_file():
            cover = file.parent.joinpath("cover.jpg")
        if not cover.is_file():
            logger.warning("No cover for %s", file)
        else:
            music.cover = open(cover, mode="rb").read()
        music.composers = [confirm_normalize(c) for c in music.composers]
        music.lyricists = [confirm_normalize(l) for l in music.lyricists]
        music.save()


def update_jlyric(file: Path, music: MusicMeta):
    metadata = jlyric.search(music.title, music.artist)
    if len(metadata.composers) > 0:
        music.composers = metadata.composers
    if len(metadata.lyricists) > 0:
        music.lyricists = metadata.lyricists
    if metadata.lyric is not None:
        music.lyrics = metadata.lyric
        lyrics_path = file.parent.joinpath(f"{file.stem}.txt")
        with open(lyrics_path, mode="w", encoding="utf-8", newline="") as f:
            f.write(metadata.lyric)


def update_dict(metadata_dict: dict, music: MusicMeta):
    matched: Optional[MusicMeta] = None
    for key, value in metadata_dict.items():
        if key in music.title:
            matched = value
            break
    if matched is None:
        logger.warning("Cannot matched %s", music.title)
        return
    logger.info("Matched %s to %s", music.title, matched.title)
    music.composers = matched.composers.copy()


def get_music_files(base_dir: Path) -> List[Path]:
    if not base_dir.is_dir():
        logger.warning("%s is not a directory", base_dir)
        return []
    files = filter_suffixes(base_dir, [".flac", ".mp3"])
    if len(files) <= 0:
        logger.warning("%s is empty", base_dir)
    return files


def filter_suffixes(path: Path, suffixes: List[str]):
    return [p for p in path.iterdir() if p.is_file() and (p.suffix.lower() in suffixes)]


def create_meta(file: Path) -> Optional[MusicMeta]:
    if file.suffix == ".flac":
        return FlacMeta(file)
    if file.suffix == ".mp3":
        return Mp3Meta(file)
    return None


if __name__ == "__main__":
    main()
